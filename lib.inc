%assign READ 0
%assign WRITE 1
%assign EXIT 60
%assign MINUS 45
%assign PARSA 0xA
%assign PARSO 0x9
%assign SPACE 0x20
%assign WORD 22
section .text


 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], READ
    je .end
    inc rax
    jmp .loop
.end:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax
    mov rax, WRITE
    mov rdi, WRITE
    pop rsi
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, PARSA


; Принимает код символа и выводит его в stdout
print_char:
    mov rax, WRITE
    push rdi
    mov rsi, rsp
    pop rdi
    mov rdi, WRITE
    mov rdx, WRITE
    syscall
    ret





; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, rsp
    sub rsp, WORD
    dec rcx
    mov [rcx], byte READ
    mov r10, 10
  .loop:
    xor rdx, rdx
    div r10
    add rdx, '0'
    dec rcx
    mov [rcx], dl
    test rax, rax
    jne .loop
    mov rdi, rcx
    call print_string
    add rsp, WORD
    ret



; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax,rax
    test rdi, rdi
    jge .print
    push rdi
    mov rdi, MINUS
    call print_char
    pop rdi
    neg rdi
.print:
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    xor r9, r9
    xor r10, r10
    mov r9b, byte[rdi+rcx]
    mov r10b, byte[rsi+rcx]
    cmp r9, r10
    jne .equalsNot
.nextchar:
    cmp byte[rdi+rcx], 0
    je .equals
    inc rcx
    jmp .loop
.equals:
    xor rax, rax
    mov rax, 1
    ret
.equalsNot:
    xor rax, rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push READ
    mov rsi, rsp
    mov rdx, WRITE
    mov rax, READ
    mov rdi, 0
    syscall
    pop rax
    ret
 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    mov r9, rdi
    mov r10, rsi
    xor rbx, rbx
.loop:
    call read_char
    cmp al, SPACE
    je .loop
    cmp al, PARSO
    je .loop
    cmp al,PARSA
    je .loop
    cmp al, READ
    je .end
.word:
    mov byte[r9 + rbx], al
    inc rbx
    call read_char
    cmp al, SPACE
    je .end
    cmp al, PARSO
    je .end
    cmp al, PARSA
    je .end
    cmp al, READ
    je .end
    cmp r10, rbx
    ja .word
.end1:
    xor rax, rax
    pop rbx
    ret
.end:
    mov rax, r9
    mov rdx, rbx
    mov byte[r9 + rbx], 0
    pop rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r10, 10
.loop:
    xor r11, r11
    mov BYTE r11b, [rdi]
    sub r11b, 48
    jb .end
    cmp r11b, 9
    ja .end
    mul r10
    add rax, r11
    inc rcx
    inc rdi
    jmp .loop
.end:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    mov BYTE r9b, [rdi]
    cmp r9b, MINUS
    jne .loop
    inc rdi
.loop:
    call parse_uint
    cmp r9b, MINUS
    jne .end
    neg rax
    inc rdx
.end:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jae .end
.loop:
    mov dl, byte[rdi]
    mov byte[rsi], dl
    inc rdi
    inc rsi
    cmp dl, 0
    jne .loop
    ret
.end:
    xor rax, rax
    ret
